<?php

namespace Musonza\Chat\Models;

use Illuminate\Database\Eloquent\Model;
use Musonza\Chat\BaseModel;
use Musonza\Chat\Chat;
use Musonza\Chat\Eventing\EventGenerator;
use Musonza\Chat\Models\Conversation;
use Musonza\Chat\Models\MessageNotification;

class Message extends BaseModel
{
    use EventGenerator;

    /**
     * @var array
     */
    protected $fillable = [
        'body',
        'user_id',
        'type'
    ];
    /**
     * @var string
     */
    protected $table = 'mc_messages';
    /**
     * All of the relationships to be touched.
     *
     * @var array
     */
    protected $touches = ['conversation'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'flagged' => 'boolean',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->belongsTo(Chat::userModelClass(), 'user_id');
    }

    /**
     * @param $user
     * @return mixed
     */
    public function unreadCount($user)
    {
        return Chat::messageNotificationModel()->where('user_id', $user->getKey())
            ->where('is_seen', 0)
            ->count();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function conversation()
    {
        return $this->belongsTo(Chat::conversationModelClass(), 'conversation_id');
    }

    /**
     * Adds a message to a conversation.
     *
     * @param Conversation $conversation
     * @param string       $body
     * @param int          $userId
     * @param string       $type
     *
     * @return Message
     */
    public function send(Model $conversation, $body, $userId, $type = 'text')
    {
        $message = $conversation->messages()->create(
            [
            'body' => $body,
            'user_id' => $userId,
            'type' => $type,
            ]
        );

        $messageWasSent = Chat::sentMessageEvent();
        $message->load('sender');
        $this->raise(new $messageWasSent($message));

        return $message;
    }

    /**
     * Deletes a message.
     *
     * @param Message $message
     * @param User    $user
     *
     * @return
     */
    public function trash($user)
    {
        return Chat::messageNotificationModel()->where('user_id', $user->getKey())
            ->where('message_id', $this->id)
            ->delete();
    }

    /**
     * Return user notification for specific message.
     *
     * @param $user
     *
     * @return MessageNotification
     */
    public function getNotification($user)
    {
        return Chat::messageNotificationModel()->where('user_id', $user->getKey())
            ->where('message_id', $this->id)
            ->select(
                [
                'mc_message_notification.*',
                'mc_message_notification.updated_at as read_at'
                ]
            )
            ->first();
    }

    /**
     * Marks message as read.
     *
     * @param User $user
     *
     * @return void
     */
    public function markRead($user)
    {
        $this->getNotification($user)->markAsRead();
    }

    /**
     * @param $user
     * @return bool
     */
    public function flagged($user)
    {
        return !!Chat::messageNotificationModel()->where('user_id', $user->getKey())
            ->where('message_id', $this->id)
            ->where('flagged', 1)
            ->first();
    }

    /**
     * @param $user
     * @return $this
     */
    public function toggleFlag($user)
    {
        Chat::messageNotificationModel()->where('user_id', $user->getKey())
            ->where('message_id', $this->id)
            ->update(['flagged' => $this->flagged($user) ? false : true]);

        return $this;
    }
}
