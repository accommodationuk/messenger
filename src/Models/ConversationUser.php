<?php

namespace Musonza\Chat\Models;

use Musonza\Chat\BaseModel;
use Musonza\Chat\Chat;

class ConversationUser extends BaseModel
{
    protected $table = 'mc_conversation_user';

    /**
     * Conversation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function conversation()
    {
        return $this->belongsTo(Chat::conversationModelClass(), 'conversation_id');
    }
}
