<?php

namespace Musonza\Chat\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Musonza\Chat\BaseModel;
use Musonza\Chat\Chat;

class MessageNotification extends BaseModel
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'message_id',
        'conversation_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->belongsTo(Chat::userModelClass(), 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function conversation()
    {
        return $this->belongsTo(Chat::conversationModelClass(), 'conversation_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function message()
    {
        return $this->belongsTo(Chat::messageModelClass(), 'message_id');
    }

    /**
     * @var string
     */
    protected $table = 'mc_message_notification';

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Creates a new notification.
     *
     * @param Message      $message
     * @param Conversation $conversation
     */
    public static function make($message, Model $conversation)
    {
        static::createCustomNotifications($message, $conversation);
    }

    /**
     * @param  $user
     * @return mixed
     */
    public function unReadNotifications($user)
    {
        return Chat::messageNotificationModel()->where(
            [
            [
                'user_id',
                '=',
                $user->getKey()
            ],
            [
                'is_seen',
                '=',
                0
            ],
            ]
        )->get();
    }

    /**
     * @param $message
     * @param $conversation
     */
    public static function createCustomNotifications($message, $conversation)
    {
        $notification = [];

        foreach ($conversation->users as $user) {
            $is_sender = ($message->user_id == $user->getKey()) ? 1 : 0;

            $notification[] = [
                'user_id'         => $user->getKey(),
                'message_id'      => $message->id,
                'conversation_id' => $conversation->id,
                'is_seen'         => $is_sender,
                'is_sender'       => $is_sender,
                'created_at'      => $message->created_at,
            ];
        }

        static::insert($notification);
    }

    /**
     *
     */
    public function markAsRead()
    {
        $this->is_seen = 1;
        $this->update(['is_seen' => 1]);
        $this->save();
    }
}
