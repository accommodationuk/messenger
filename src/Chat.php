<?php

namespace Musonza\Chat;

use Illuminate\Database\Eloquent\Model;
use Musonza\Chat\Models\Conversation;
use Musonza\Chat\Models\ConversationUser;
use Musonza\Chat\Models\Message;
use Musonza\Chat\Models\MessageNotification;
use Musonza\Chat\Services\ConversationService;
use Musonza\Chat\Services\MessageService;
use Musonza\Chat\Traits\SetsParticipants;

class Chat
{
    use SetsParticipants;

    /**
     * @param MessageService      $messageService
     * @param ConversationService $conversationService
     */
    public function __construct(MessageService $messageService, ConversationService $conversationService)
    {
        $this->messageService = $messageService;
        $this->conversationService = $conversationService;
        $this->messageNotification = Chat::messageNotificationModel();
    }

    /**
     * Creates a new conversation.
     *
     * @param array $participants
     * @param array $data
     *
     * @return Conversation
     */
    public function createConversation(array $participants, array $data = [])
    {
        return $this->conversationService->start($participants, $data);
    }

    /**
     * Sets message.
     *
     * @param string | Musonza\Chat\Models\Message $message
     *
     * @return MessageService
     */
    public function message($message)
    {
        return $this->messageService->setMessage($message);
    }

    /**
     * Gets MessageService.
     *
     * @return MessageService
     */
    public function messages()
    {
        return $this->messageService;
    }

    /**
     * Sets Conversation.
     *
     * @param  Conversation $conversation
     * @return ConversationService
     */
    public function conversation(Model $conversation)
    {
        return $this->conversationService->setConversation($conversation);
    }

    /**
     * Gets ConversationService.
     *
     * @return ConversationService
     */
    public function conversations()
    {
        return $this->conversationService;
    }

    /**
     * Get unread notifications.
     *
     * @return MessageNotification
     */
    public function unReadNotifications()
    {
        return $this->messageNotification->unReadNotifications($this->user);
    }

    /**
     * Returns the User Model class.
     *
     * @return string
     */
    public static function userModelClass()
    {
        return config('musonza_chat.models.user');
    }

    /**
     * Returns the User Model class.
     *
     * @return string
     */
    public static function userModel()
    {
        return app(static::userModelClass());
    }

    /**
     * Returns the Messenge Model class.
     *
     * @return string
     */
    public static function messageModelClass()
    {
        return config('musonza_chat.models.message', Message::class);
    }

    /**
     * Returns the Message Model instance.
     *
     * @return string
     */
    public static function messageModel()
    {
        return app(static::messageModelClass());
    }

    /**
     * Returns the Conversation Model class.
     *
     * @return string
     */
    public static function conversationModelClass()
    {
        return config('musonza_chat.models.conversation', Conversation::class);
    }

    /**
     * Returns the Conversation Model instance.
     *
     * @return string
     */
    public static function conversationModel()
    {
        return app(static::conversationModelClass());
    }

    /**
     * Returns the ConversationUser Model class.
     *
     * @return string
     */
    public static function conversationUserModelClass()
    {
        return config('musonza_chat.models.conversationUser', ConversationUser::class);
    }

    /**
     * Returns the ConversationUser Model instance.
     *
     * @return Model
     */
    public static function conversationUserModel()
    {
        return app(static::conversationUserModelClass());
    }

    /**
     * Returns the MessageNotification Model class.
     *
     * @return string
     */
    public static function messageNotificationModelClass()
    {
        return config('musonza_chat.models.messageNotification', MessageNotification::class);
    }

    /**
     * Returns the MessageNotification Model instance.
     *
     * @return Model
     */
    public static function messageNotificationModel()
    {
        return app(static::messageNotificationModelClass());
    }

    /**
     * Returns primary key for the User model
     *
     * @return string
     */
    public static function userModelPrimaryKey()
    {
        return config('musonza_chat.user_model_primary_key') ?: app(self::userModelClass())->getKeyName();
    }

    /**
     * Should the messages be broadcasted.
     *
     * @return boolean
     */
    public static function broadcasts()
    {
        return config('musonza_chat.broadcasts');
    }

    public static function sentMessageEvent()
    {
        return config('musonza_chat.sent_message_event');
    }

    public static function makeThreeOrMoreUsersPublic()
    {
        return config('musonza_chat.make_three_or_more_users_public', true);
    }
}
